package com.huawei.servicecomb.controller;



import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class TestProject1j84 {

        Project1j84Delegate project1j84Delegate = new Project1j84Delegate();


    @Test
    public void testhelloworld(){

        String expactReturnValue = "hello"; // You should put the expect String type value here.

        String returnValue = project1j84Delegate.helloworld("hello");

        assertEquals(expactReturnValue, returnValue);
    }

}